package com.nalinthip.midterm;

public class CinemaTicket {
    private String name;
    private int seats;
    private int price;

    public CinemaTicket(String name, int seats, int price) {
        this.name = name;
        this.seats = seats;
        this.price = price;

    }
    
    public String getName() {
        return name;
    }

    public int getSeats() {
        return seats;
    }

    public int getPrice() {
        return price * seats;
    }

    public void print() {
        System.out.println();
    }
}
