package com.nalinthip.midterm;

import java.util.Scanner;

public class FoodDispenserApp {
    static void printFoodDispense() {
        System.out.println("------------- Type of Product -------------");
        System.out.println("snack | Chocolate");
        System.out.println("bread | Sandwich");
        System.out.println("water | Honey Lemon Drink");
        System.out.println("ok to Satisfied with the order");
    }

    public static void main(String[] args) {
        while (true) {
            String type = chooseType();
            choicetype(type);
        }
        
    }

    static FoodDispenser snack = new FoodDispenser("snack", "chocolate", 45);
    static FoodDispenser bread = new FoodDispenser("bread", "sandwich", 25);
    static FoodDispenser water = new FoodDispenser("water", "Honey Lemon Drink", 35);
    public static String chooseType() {
        printFoodDispense();    
        String type;
        System.out.print("Please input type of Product : ");
        Scanner sc = new Scanner(System.in);
        type = sc.next();
        return type;
    }

    public static int inputnumberofpieces() {

        String strCount;
        int count;
        System.out.print("Please input count : ");
        Scanner sc = new Scanner(System.in);
        strCount = sc.next();
        count = Integer.parseInt(strCount);
        return count;
    }

    public static void choicetype(String type) {
        int count;
        switch (type) {
            case "snack":
                snack.print();
                count = inputnumberofpieces();
                snack.printTotal(count);
                break;
            case "bread":
                bread.print();
                count = inputnumberofpieces();
                bread.printTotal(count);
                break;
            case "water":
                water.print();
                count = inputnumberofpieces();
                water.printTotal(count);
                break;
            case "ok":
                System.out.println("---------------- Satisfied with the order ----------------");
                System.exit(0);
                break;
        }
    }
    
}
