package com.nalinthip.midterm;

import java.util.Scanner;

public class CinemaTicketApp {
    static void print() {
        System.out.println("------------- Please choose Ticket -------------");
        System.out.println("Ticket seats1 | Normal 180 Baht [push1]");
        System.out.println("Ticket seats2 | Honeymoon 260 Baht [push2]");
        System.out.println("Ticket seats3 | Opera Chair 500 Baht [push3]");

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        CinemaTicket seatsNormal = new CinemaTicket("seats Normal", 180, 1);
        CinemaTicket seatsHoneymoon = new CinemaTicket("seats Honeymoon", 260, 1);
        CinemaTicket seatsOperaChair = new CinemaTicket("seats OperaChair", 500, 1);
        print();
        System.out.print("Choose your seats  = ");
        int choose = sc.nextInt();
        System.out.print("Choose how many seats = ");
        int seats = sc.nextInt();

        int price;
        if (choose == 1) {
            System.out.print("Seat of your choice : ");
            System.out.println(seatsNormal.getName());
            System.out.println("----------- price = " + seatsNormal.getPrice() * seats + " Baht -----------");
        } else if (choose == 2) {
            System.out.print("Please enter your seats : ");
            System.out.println(seatsHoneymoon.getName());
            System.out.println("         price = " + seatsHoneymoon.getPrice() * seats);
        } else if (choose == 3) {
            System.out.print("Please enter your seats : ");
            System.out.println(seatsOperaChair.getName());
            System.out.println("         price = " + seatsOperaChair.getPrice() * seats);
        }

    }
}
