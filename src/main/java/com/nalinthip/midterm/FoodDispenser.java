package com.nalinthip.midterm;

public class FoodDispenser {
    private String typeOfProduct;
    private String productName;
    private double price;

    public FoodDispenser(String typeOfProduct, String productName, double price) {
        this.typeOfProduct = typeOfProduct;
        this.productName = productName;
        this.price = price;
    }

    public void printall() {
        System.out.println("TypeOfProduct : " + typeOfProduct);
        System.out.println("ProductName : " + productName);
        System.out.println("Coet : " + price + " Dollar ");
    }

    public void printTotal(int count) {
        double total = price * count;
        System.out.println(productName + " : Number " + count + " piece  ||  Cost " + total + " Dollar ");
    }
    public void print() {
        System.out.println();
    }
}